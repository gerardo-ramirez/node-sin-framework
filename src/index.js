const http = require('http');
const {myBodyParse} = require('./lib/myBodyParse')
//simulamos una base de datos:
let database = [];
//cortamos y pegamos la funcion de /tasks
const getTasksHandler = (req,res)=>{
    res.writeHead(200,{'Content-Type': 'application/json'});
    res.write(JSON.stringify(database));
    res.end();
};
const createTasksHandler = async (req,res)=>{
    try{
        await myBodyParse(req);
        console.log(req.body)
        database.push(req.body);
        res.writeHead(200,{'Content-Type': 'application/json'});
        res.write(JSON.stringify(database));
        res.end();


    } catch(error){
      
        res.writeHead(200,{'Content-Type': 'text/plain'});
        res.write('invalid Data');
        console.log(error);
        res.end();

    }
  
};
const updateTaskHandler= async (req, res)=>{
    try {
        let { url } = req;
    
        let idQuery = url.split("?")[1];
        let idKey = idQuery.split("=")[0];
        let idValue = idQuery.split("=")[1];
    
        if (idKey === "id") {
          await mybodyParse(req);
          database[idValue - 1] = req.body;
          res.writeHead(200, { "Content-Type": "application/json" });
          res.write(JSON.stringify(database));
          res.end();
        } else {
          res.writeHead(200, { "Content-Type": "text/plain" });
          res.write("Invalid Request Query");
          res.end();
        }
      } catch (err) {
        response.writeHead(400, { "Content-type": "text/plain" });
        response.write("Invalid body data was provided", err.message);
        response.end();
      }


};
async function deleteTaskHandler(req, res) {
    let { url } = req;
  
    let idQuery = url.split("?")[1];
    let idKey = idQuery.split("=")[0];
    let idValue = idQuery.split("=")[1];
  
    if (idKey === "id") {
      database.splice(idValue - 1, 1);
      res.writeHead(200, { "Content-type": "text/plain" });
      res.write("Delete Success");
      res.end();
    } else {
      res.writeHead(400, { "Content-type": "text/plain" });
      res.write("Invalid Query");
      res.end();
    }
  };



//seteamos un servidor sin express:
const server = http.createServer((req,res)=>{
const {url, method }= req;
//vemos las peticiones :
console.log(`URL: ${url} - METHOD: ${method}`);




//colocamos un swich para trabajar cada Method:
switch(method){
    case "GET":
    if(url === "/"){
        //las cabeceras nos permiten especificar el tipo de archivos:
//esto hace entre otras cosas que puedas abrir pdf , escuchar mp3 desde el navegador.
res.writeHead(200,{'Content-Type': 'application/json'});
res.write(JSON.stringify({message: 'hello word'}));
res.end();

    }
    if(url === "/tasks"){
  
        getTasksHandler(req, res);

    }

    break;
     case "POST":
         if(url === "/tasks"){
             createTasksHandler(req,res);
         }
         case "PUT":
            updateTaskHandler(req, res);
            break;
          case "DELETE":
            deleteTaskHandler(req, res);
            break;
          default:
            res.writeHead(404, { "Content-Type": "text/plain" });
            res.write("404 Not Found");
            res.end();
   


}



});
//este recibe un puerto : 
server.listen(3000);
console.log('server on port 3000');